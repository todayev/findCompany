# findCompany
```
docker build -t find_company .
docker run find_company node index.js <company`s ITN>
```
or
```
docker run registry.gitlab.com/todayev/findcompany:latest node index.js <company`s ITN>
```

# example
```
docker run registry.gitlab.com/todayev/findcompany:latest node index.js 7736216869
```

**result:**
```
[
  {
    id: 573,
    name: 'ПАО "ФосАгро"',
    district: 'Центральный',
    region: 'Москва',
    branch: 'Иное',
    lastActivity: '2023-08-31T17:00:00.067',
    docCount: 256,
    url: 'https://e-disclosure.ru/portal/company.aspx?id=573'
  }
]
```
