const USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0";

async function findCompany(ITN) {
  const url_start= "https://e-disclosure.ru/poisk-po-kompaniyam";
  const url_api = "https://e-disclosure.ru/api/search/companies";
  const url_company = "https://e-disclosure.ru/portal/company.aspx?id=";
  const body = `textfield=${ITN}&radReg=FederalDistricts&districtsCheckboxGroup=-1&regionsCheckboxGroup=-1&branchesCheckboxGroup=-1&lastPageSize=10&lastPageNumber=1&query=${ITN}`;
  let cookie  = "";
  return fetch(url_start, {
    method: "GET",
    redirect: "manual",
    headers: {
      "User-Agent": USER_AGENT,
    }
  }).then(res => {
    cookie = res.headers
      .getSetCookie()
      .map(cook => cook.split(";")[0])
      .join("; ");
  })
  .then(() => {
    return fetch(
      url_api,
      {
        method:  "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
          "User-Agent": USER_AGENT,
          cookie
        },
        body
      }
    )
  }).then(res => res.json())
  .then(json => {
    return json.foundCompaniesList?.map(company => {
      return {
        ...company,
        url: `${url_company}${company.id}`
      }
    }) || json.foundCompaniesList
  })
  .catch(err =>  console.error(err));
}


findCompany(process.argv[2]).then((result) => {
  console.log(result);
})
